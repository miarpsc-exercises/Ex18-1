# Spring 2018 Drill

Plans and documents for the Spring 2018 drill

----

It has been decided that the Spring exercise will essentially be a
repeat of the September SET. This will give counties an opportunity to
test updated plans, and perhaps share some of the deltas from SET with
their programs and see how well they have improved.

Possible Dates:

* March 3 ARRL International DX Contest Phone
* March 10 AGCW QRP Contest
* March 17 Crossroads Hamfest
* March 24 *NWS/DTX*, CQ WW WPX Contest, SSB
* March 31 *Easter weekend*
* April 7 D3 EC meeting, SKCC Weekend Sprintathon
* April 14
* April 21 *Michigan QSO Party*
* April 28 10-10 Int. Spring Contest Digital
* May 5 10-10 Int. Spring Contest CW
* May 12 Mother's Day weekend, SKCC Weekend Sprintathon, FISTS Spring Unlimited Sprint
* May 19 *Hamvention*
* May 26 CQ WW WPX Contest CW
* June 2 10-10 Int. Open Season PSK Contest
* June 9 June VHF contest, D3 EC meeting, SKCC Weekend Sprintathon, ARRL June VHF Contest
* June 16 Midland Hamfest
* June 23 *Field Day*
* June 30


