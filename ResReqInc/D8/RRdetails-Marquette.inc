<?php
$Typed=1;
$Resource_Name = "Law Enforcement Patrol Team (Strike Team) Type 4";
$Quantity = 3;
$Units = 10;
$Mission[1]="Quell rioting at the Junction 95 Tavern & Grill";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="11522 US-41, Champion, MI 49814";
$Sources[1]="Michigan State Police";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 14:30";
$Objective = "2. Maintain public order";
$POC="Mary H. A. L. Lamb";
$POCnum="906-339-4195";
$Assigned="SEOC LOG Logistics Section Chief";
?>
