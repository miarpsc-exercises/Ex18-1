<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Need to evacuate the residents of Horton Bay";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="4961 Boyne City Rd, Boyne City, MI 49712";
$Sources[1]="LSC";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Lt. Edna Hollowell";
$POCnum="231-582-9261";
$Assigned="SEOC LOG Logistics Section Chief";
?>
