<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Evacuate stranded hikers from the Reeds";
$Mission[2]="Alverno Nature Preserve";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="5012 Orchard Beach Rd, Cheboygan, MI 49721";
$Sources[1]="Michigan State Police";
$Sources[2]="Logistics Section Chief";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Lt. Peggy Louchart";
$POCnum="231-625-9176";
$Assigned="SEOC LOG Logistics Section Chief";
?>
