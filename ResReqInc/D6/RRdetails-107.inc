<?php
$Typed=1;
$Resource_Name = "Ambulance Strike Team Type 4";
$Quantity = 1;
$Units = 10;
$Mission[1]="Large number of patients must be transported from";
$Mission[2]="the St. Ives Glof Club to area hospitals.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="9900 St Ives Dr, Stanwood, MI 49346";
$Sources[1]="Michigan Department of Health and Human Services";
$Sources[2]="Region 6 Healthcare Coalition";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "1. Maintain safety of life and property";
$POC="Ms. Ragnhild Oyvind ";
$POCnum="231-972-7155";
$Assigned="SEOC LOG Logistics Section Chief";
?>
