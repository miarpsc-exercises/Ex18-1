<?php
$Typed=1;
$Resource_Name = "Paramedic Type 1";
$Quantity = 4;
$Units = 14;
$Mission[1]="Treat victims of recent Jaxartosaurus attacks at";
$Mission[2]="Clayton lumber";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="11029 Center St, Clayton, MI 49235";
$Sources[1]="Region 1 Healthcare Coalition";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Dr. Helmut Franck";
$POCnum="517-445-2617";
$Assigned="SEOC LOG Logistics Section Chief";
?>
