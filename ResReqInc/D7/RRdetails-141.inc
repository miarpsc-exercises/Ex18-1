<?php
$Typed=1;
$Resource_Name = "Ambulance Strike Team Type 4";
$Quantity = 1;
$Units = 10;
$Mission[1]="Require transport of patients from the region";
$Mission[2]="of Ocqueoc to area medical facilities";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="14072 N Allis Hwy, Millersburg, MI 49759";
$Sources[1]="Region 7 Healthcare Coalition";
$Sources[2]="Michigan Department of Health and Human Services";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Valda van der Westhuizen";
$POCnum="989-733-7189";
$Assigned="SEOC LOG Logistics Section Chief";
?>
