<?php
$Typed=1;
$Resource_Name = "Law Enforcement Patrol Team (Strike Team) Type 4";
$Quantity = 3;
$Units = 10;
$Mission[1]="Crowd control around \"Sledheads of Frederic\"";
$Mission[2]="Old 27 and Myrtle Street";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="6547 Frederic St, Frederic, MI 49733";
$Sources[1]="MSP";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "2. Maintain public order";
$POC="Deputy Jesse James";
$POCnum="989-348-8190";
$Assigned="SEOC LOG Logistics Section Chief";
?>
