<?php
$Typed=1;
$Resource_Name = "Airborne Communications Relay Team (Fixed-Wing) Type 4";
$Quantity = 1;
$Units = 12;
$Mission[1]="Provide repeater support for search and rescue";
$Mission[2]="operations around Markin Glen Park.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="7459 N Riverview Dr, Kalamazoo, MI 49004 - 2H4";
$Sources[1]="Civil Air Patrol";
$Sources[2]="Michigan State Police";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "4. Maintain operational awareness to the extent possible";
$POC="Gerald R. Chevrolet";
$POCnum="269-381-8983";
$Assigned="SEOC LOG Logistics Section Chief";
?>
