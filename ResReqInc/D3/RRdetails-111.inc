<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Coordinate evacuation from 11th and E St";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="111 W Main St, Midland, MI 48640";
$Sources[1]="Logistics";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "1. Maintain safety of life and property";
$POC="Ms. Jennifer Boyer";
$POCnum="989-839-9911";
$Assigned="SEOC LOG Logistics Section Chief";
?>
