<?php
$Typed=1;
$Resource_Name = "Animal Protection: Small Animal Sheltering Team-Type 3";
$Quantity = 2;
$Units = 12;
$Mission[1]="Provide shelter for a large number of";
$Mission[2]="exposed red chipmunks";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="10505 S Hemlock Rd, Brant, MI 48614";
$Sources[1]="Michigan Department of Agriculture and Rural development";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Elmer J. Fudd";
$POCnum="989-245-0085";
$Assigned="SEOC LOG Logistics Section Chief";
?>
