<?php
$Typed=1;
$Resource_Name = "Paramedic Type 1";
$Quantity = 4;
$Units = 14;
$Mission[1]="Paramedics required to assist in treating injured";
$Mission[2]="cyclists at the Hart-Montague Bicycle Trail Park.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="Rest Area 530, SB 31, Shelby, MI 49455";
$Sources[1]="Region 6 Health Care Coalition";
$Sources[2]="Michigan Department of Health and Human Services";
$Sources[3]="";
$DateTime="2018-04-28 10:00";
$Objective = "1. Maintain safety of life and property";
$POC="Dr. Baltazar X. Augustyniak";
$POCnum="231-861-4212";
$Assigned="SEOC LOG Logistics Section Chief";
?>
