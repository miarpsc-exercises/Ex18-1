<?php
$Typed=1;
$Resource_Name = "Law Enforcement Patrol Team (Strike Team) Type 4";
$Quantity = 3;
$Units = 10;
$Mission[1]="Large groups of environmental terrorists are gathering";
$Mission[2]="around the Michigamme Reservoir. Law enforcement";
$Mission[3]="assistance is required to maintain order.";
$Mission[4]="";
$Reporting_Location="368 Kopf's Rd, Crystal Falls, MI 49920";
$Sources[1]="Michigan State Police";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "2. Maintain public order";
$POC="F/Lt. Archibald Q. E. Cox";
$POCnum="906-875-3862";
$Assigned="SEOC LOG Logistics Section Chief";
?>
