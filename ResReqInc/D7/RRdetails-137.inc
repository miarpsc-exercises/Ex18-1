<?php
$Typed=1;
$Resource_Name = "Paramedic Type 1";
$Quantity = 4;
$Units = 14;
$Mission[1]="Assist with injured personnel near Whites Landing.";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="7755 Turtle Lake Rd, Johannesburg MI 49751";
$Sources[1]="Region 7 Healthcare Coalition";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "1. Maintain safety of life and property";
$POC="Lt. Agafya Vyacheslav";
$POCnum="989-732-5929";
$Assigned="SEOC LOG Logistics Section Chief";
?>
