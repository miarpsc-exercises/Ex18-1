<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Assist in evacuation of the city of Luna Pier.";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="4163 Super Eight Dr, Luna Pier, MI 48157";
$Sources[1]="Logistics Section Chief";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "1. Maintain safety of life and property";
$POC="Ms. Marianne S. Miller";
$POCnum="734-848-8687";
$Assigned="SEOC LOG Logistics Section Chief";
?>
