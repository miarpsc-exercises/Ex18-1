<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Assist in evacuation of large numbers of stranded";
$Mission[2]="citizens from the Brighton Recreation Area.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="5415 Mystic Lake Dr, Brighton, MI 48116";
$Sources[1]="Logistics Section";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Sgt. R. Bilko";
$POCnum="810-227-6302";
$Assigned="SEOC LOG Logistics Section Chief";
?>
