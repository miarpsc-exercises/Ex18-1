<?php
$Typed=1;
$Resource_Name = "Ambulance Strike Team Type 4";
$Quantity = 1;
$Units = 10;
$Mission[1]="Additional resources required to transport large";
$Mission[2]="numbers of injured victims to area hospitals.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="1715 Lansing Ave #221, Jackson, MI 49202";
$Sources[1]="Michigan Department of Health and Human Services";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "1. Maintain safety of life and property";
$POC="Dr. Haley Mills";
$POCnum="517-788-3215";
$Assigned="SEOC LOG Logistics Section Chief";
?>
