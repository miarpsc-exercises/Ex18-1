<?php
$Typed=1;
$Resource_Name = "Mobile Field Force Law Enforcement (Crowd Control Teams)";
$Quantity = 1;
$Units = 10;
$Mission[1]="Unruly crowds are gathering at the Food Bank of";
$Mission[2]="Eastern Michigan for presumed looting. Law enforcement";
$Mission[3]="support is required";
$Mission[4]="";
$Reporting_Location="2300 Lapeer Rd, Flint, MI 48503";
$Sources[1]="Michigan State Police";
$Sources[2]="Genesee County Sheriff Department";
$Sources[3]="";
$DateTime="2018-04-28 10:00";
$Objective="2. Maintain public order";
$POC="Sgt. Wilbur E. Spore";
$POCnum="(810) 123-4567";
$Assigned="SEOC OPS MSP";
?>
