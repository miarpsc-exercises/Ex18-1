<?php
$Typed=1;
$Resource_Name = "Emergency Medical Task Force Leader Type 1";
$Quantity = 1;
$Units = 4;
$Mission[1]="Lead a task force of medical professionals to set up";
$Mission[2]="a field trauma center for victims of chemical burns";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="6721 East Elm St, Auburn, MI 48611";
$Sources[1]="Local hospitals";
$Sources[2]="Local medical association";
$Sources[3]="MDHHS";
$DateTime="2018-04-28 13:00";
$Objective="1. Maintain safety of life and property";
$POC="James Earl Ray";
$POCnum="(989) 123-4567";
$Assigned="SEOC OPS Human Services Branch Director";
?>
