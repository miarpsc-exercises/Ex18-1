<?php
$Typed=1;
$Resource_Name = "Area Command Team, Firefighting";
$Quantity = 1;
$Units = 12;
$Mission[1]="A fire has broken out at MCN Oil & Gas which exceeds";
$Mission[2]="the ability of local resources to contain. Additional";
$Mission[3]="support is required";
$Mission[4]="";
$Reporting_Location="MCN Oil & Gas, 2330 Lewis Rd, East Jordan, MI 49727";
$Sources[1]="County of Cheboygan Fire Service";
$Sources[2]="County of Grand Traverse Hazardous Materials";
$Sources[3]="SEOC OPS Operations Section Chief";
$DateTime="2018-04-28 10:00";
$Objective="1. Maintain safety of life and property";
$POC="Chief Jesse Lane";
$POCnum="(231) 544-0123";
$Assigned="";
?>
