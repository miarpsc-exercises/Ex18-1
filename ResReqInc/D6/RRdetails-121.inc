<?php
$Typed=1;
$Resource_Name = "Field Kitchen Unit Type 4";
$Quantity = 2;
$Units = 4;
$Mission[1]="Feed residents of Montague, Whitehall and Lakewood";
$Mission[2]="Club who have been without power for some time.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="5353 Wilcox St, Montague, MI 49437";
$Sources[3]="First Baptist Church";
$Sources[2]="American Red Cross";
$Sources[1]="Mennonite Disaster Services";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Ragnhildur Holmberg";
$POCnum="231-894-9851";
$Assigned="SEOC LOG Logistics Section Chief";
?>
