<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Evacuate Zug Island";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="8676 Dearborn St, Detroit, MI 48209";
$Sources[1]="Logistics Section";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 11:00";
$Objective = "1. Maintain safety of life and property";
$POC="Cpt. Valerie Yurgaites";
$POCnum="313-436-5386";
$Assigned="SEOC LOG Logistics Section Chief";
?>
