<?php
$Typed=1;
$Resource_Name = "Species Specialist-Type 1";
$Quantity = 1;
$Units = 4;
$Mission[1]="Provide expertise to deter attacks of Argente";
$Mission[2]="de Champagne (Champagne d'Argent) rabbits";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="254 N Michigan Ave, Beulah, MI 49617";
$Sources[1]="Michigan Department of Agriculture and Rural Development";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="F/Lt Sue Smokovitz";
$POCnum="231-383-5216";
$Assigned="SEOC LOG Logistics Section Chief";
?>
