<?php

function NextPage($pdf)
{
      $pdf->newPage();
}


function centerText($pdf,$l,$r,$y,$fs,$text)
{
    $slen=$pdf->GetStringWidth($text);
    $XPos = round(($l+$r)/2 - $slen/2 );
    //$msg = 'Len ' . $slen . ', y=' . $y . ', text=' . $text;
    //$pdf->addText(200,300,10,$msg);
    //$msg = 'l,r=' . $l . ',' . $r . ', Pos ' . $XPos;
    //$pdf->addText(200,340,10,$msg);
    $pdf->addText($XPos,$y,$fs,$text);
}

function headingFont( $pdf )
{
    $pdf->SelectFont("helvetica-Bold");
}

function subheadFont( $pdf )
{
    $pdf->SelectFont("helvetica-Bold");
}

function defaultFont( $pdf )
{
    $pdf->SelectFont("helvetica");
}

function makeCheckbox( $pdf, $x, $y )
{
    $pdf->Line($x,  $y,  $x+8,$y  );
    $pdf->Line($x,  $y+8,$x+8,$y+8);
    $pdf->Line($x,  $y,  $x,  $y+8);
    $pdf->Line($x+8,$y,  $x+8,$y+8);
}

?>
