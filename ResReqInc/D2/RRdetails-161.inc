<?php
$Typed=1;
$Resource_Name = "Airborne Communications Relay Team (Fixed-Wing) Type 4";
$Quantity = 1;
$Units = 12;
$Mission[1]="Provide communications relay support to search";
$Mission[2]="operations around Bruin Lake in Unandilla";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="7150 Strawberry Lake Rd, Dexter, MI 48130 2E8";
$Sources[1]="Civil Air Patrol";
$Sources[2]="Michigan State Police";
$Sources[3]="";
$DateTime="2018-04-28 08:30";
$Objective = "4. Maintain operational awareness to the extent possible";
$POC="Col. Elizabeth Sparks";
$POCnum="734-426-4493";
$Assigned="SEOC LOG Logistics Section Chief";
?>
