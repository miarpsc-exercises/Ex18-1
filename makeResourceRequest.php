<?php

  /* Create a fillable resource request form
   */


include ('includes/class.pdf.php');
include ('includes/basicFunctions.php');
include ('includes/RadiogramHeaders.php');

$Example=1;

function bodyTextColor( $pdf )
{
  global $Example;
  if ( $Example )
    $pdf->SetTextColor(196,196,196);
  else
    $pdf->SetTextColor(0,0,0);  
}

function variableTextColor( $pdf )
{
  $pdf->SetTextColor(0,0,128);
}

function warnTextColor( $pdf )
{
  $pdf->SetTextColor(196,0,0);
}

function bodyDrawColor( $pdf )
{
  global $Example;
  if ( $Example )  
    $pdf->SetDrawColor(196,196,196);
  else
    $pdf->SetDrawColor(0,0,0);
}

function requiredColor( $pdf )
{
  $pdf->SetTextColor(196,176,176);
}

function DocumentSetup($pdf,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
                       $Left_Margin,$Right_Margin,$Month)
{
    $PageSize = array(0,0,$Page_Width,$Page_Height);
    $pdf = & new Cpdf($PageSize);

    $PageSize = array(0,0,$Page_Width,$Page_Height);
    $pdf = & new Cpdf($PageSize);

    $pdf->addinfo('Author','John J. McDonough ' . "WB8RCR");
    $pdf->addinfo('Creator','makeRR.php $Revision: 1.0$');
    $pdf->SetKeywords('ARPSC, ARES, RACES, NTS, Michigan Section');
    $pdf->selectFont('helvetica');

    $pdf->addinfo('Title',_(' MI-CIMS Resource Request'));
    $pdf->addinfo('Subject',_('Fillable MI-CIMS Resource Request') );
    $pdf->addInfo('Modified',_('2018-01-05 16:25:35Z') );

    return $pdf;

}

function boxNumber($pdf, $y, $n)
{
    defaultFont( $pdf );
    $pdf->SetTextColor(0,196,0);
    $pdf->addText(202,$y-8,8,$n . ".");
    bodyTextColor($pdf);
    subheadFont($pdf);
}

function addRequired( $pdf, $y )
{
    requiredColor($pdf);    $pdf->addText(540,$y-10,10,"Required");
    bodyTextColor($pdf);
}


date_default_timezone_set('America/Detroit');
$PaperSize = 'letter';
$Page_Width=612;
$Page_Height=792;
$Top_Margin=30;
$Bottom_Margin=57;
$Left_Margin=30;
$Right_Margin=25;
$Month=12;
$PageSize = array(0,0,$Page_Width,$Page_Height);
$pdf = & new Cpdf($PageSize);

$pdf = DocumentSetup($pdf,$Page_Width,$Page_Height,$Top_Margin,$Bottom_Margin,
                     $Left_Margin,$Right_Margin,$Month);

$PageNumber = 0;

bodyDrawColor($pdf);$pdf->SetLineWidth( 3 );

$Top_Box=$Page_Height-$Top_Margin-36; /* 1"? */
$Right_Box=$Page_Width-$Right_Margin;

RGheader($pdf);
bodyTextColor($pdf);
bodyDrawColor($pdf);
headingFont( $pdf );
$Ypos = $Top_Box-30;
centerText($pdf,0,$Right_Box,$Ypos+10,16,
	   "RESOURCE REQUEST");

$pdf->SetLineWidth( 2 );
$Subhead_Size=12;
subheadFont( $pdf );
$Default_Size=10;

$pdf->SetLineWidth( 1 );
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->Line($Left_Margin+5,$Ypos-30,$Right_Box,$Ypos-30);
$pdf->Line($Left_Margin+5,$Ypos-60,$Right_Box,$Ypos-60);
$pdf->Line($Left_Margin+5,$Ypos,$Left_Margin+5,$Ypos-60);
$pdf->Line($Right_Box,$Ypos,$Right_Box,$Ypos-60);
$pdf->Line($Left_Margin+230,$Ypos,$Left_Margin+230,$Ypos-60);
$pdf->addText($Left_Margin+10,$Ypos-20,16,"Resource Request Number:");
$pdf->addText($Left_Margin+105,$Ypos-50,16,"Incident Name:");
defaultFont( $pdf );
$pdf->SetTextColor(192,192,192);
$pdf->addText($Left_Margin+320,$Ypos-20,$Default_Size,
	      "Automaticaly filled in by MI-CIMS");

$pdf->SetTextColor(0,196,0);
$pdf->addText($Left_Margin+232,$Ypos-38,8,"1.");

bodyTextColor($pdf);

$Subhead_Size=12;
subheadFont( $pdf );

$pdf->SetFillColor(230,230,230);
$pdf->Rect(35,164,552,26,'F');
$pdf->SetLineWidth( 1 );

$Ypos-=65;
centerText($pdf,$Left_Margin,$Right_Box,$Ypos-20,$Subhead_Size,
	   "Resource Request Details");

$pdf->Line($Left_Margin+170,$Ypos-35,$Left_Margin+170,$Ypos-600);
$pdf->Line($Left_Margin+5,$Ypos-35,$Left_Margin+5,$Ypos-600);
$pdf->Line($Right_Box,$Ypos-35,$Right_Box,$Ypos-600);

$Ypos-=35;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+80,$Ypos-20,$Default_Size,"Typed Resource:");
$pdf->addText($Left_Margin+200,$Ypos-20,$Default_Size,"    Yes");
$pdf->addText($Left_Margin+380,$Ypos-20,$Default_Size,"    No");
makeCheckbox($pdf,$Left_Margin+200,$Ypos-20);
makeCheckbox($pdf,$Left_Margin+380,$Ypos-20);
boxNumber($pdf,$Ypos,2);

$Ypos-=30;
requiredColor($pdf);
$pdf->addText($Left_Margin+510,$Ypos-10,$Default_Size,"Required");
bodyTextColor($pdf);
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+80,$Ypos-20,$Default_Size,"Resource Name:");
boxNumber($pdf,$Ypos,3);

$Ypos-=30;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+120,$Ypos-20,$Default_Size,"Quantity:");
boxNumber($pdf,$Ypos,4);

$Ypos-=30;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->Line($Left_Margin+5,$Ypos-110,$Right_Box,$Ypos-110);
$pdf->addText($Left_Margin+135,$Ypos-20,$Default_Size,"Units:");

makeCheckbox($pdf,$Left_Margin+180,$Ypos-20);
makeCheckbox($pdf,$Left_Margin+300,$Ypos-20);
$pdf->addText($Left_Margin+300,$Ypos-20,$Default_Size,"    Gallons");
makeCheckbox($pdf,$Left_Margin+420,$Ypos-20);
$pdf->addText($Left_Margin+420,$Ypos-20,$Default_Size,"    Strike Team");

makeCheckbox($pdf,$Left_Margin+180,$Ypos-40);
$pdf->addText($Left_Margin+180,$Ypos-40,$Default_Size,"    Bottles");
makeCheckbox($pdf,$Left_Margin+300,$Ypos-40);
$pdf->addText($Left_Margin+300,$Ypos-40,$Default_Size,"    Other");
makeCheckbox($pdf,$Left_Margin+420,$Ypos-40);
$pdf->addText($Left_Margin+420,$Ypos-40,$Default_Size,"    Task Force");

makeCheckbox($pdf,$Left_Margin+180,$Ypos-60);
$pdf->addText($Left_Margin+180,$Ypos-60,$Default_Size,"    Boxes");
makeCheckbox($pdf,$Left_Margin+300,$Ypos-60);
$pdf->addText($Left_Margin+300,$Ypos-60,$Default_Size,"    Pallets");
makeCheckbox($pdf,$Left_Margin+420,$Ypos-60);
$pdf->addText($Left_Margin+420,$Ypos-60,$Default_Size,"    Teams");

makeCheckbox($pdf,$Left_Margin+180,$Ypos-80);
$pdf->addText($Left_Margin+180,$Ypos-80,$Default_Size,"    Cases");
makeCheckbox($pdf,$Left_Margin+300,$Ypos-80);
$pdf->addText($Left_Margin+300,$Ypos-80,$Default_Size,"    Pounds");
makeCheckbox($pdf,$Left_Margin+420,$Ypos-80);
$pdf->addText($Left_Margin+420,$Ypos-80,$Default_Size,"    Tons");

makeCheckbox($pdf,$Left_Margin+180,$Ypos-100);
$pdf->addText($Left_Margin+180,$Ypos-100,$Default_Size,"    Each");
makeCheckbox($pdf,$Left_Margin+300,$Ypos-100);
$pdf->addText($Left_Margin+300,$Ypos-100,$Default_Size,"    Roll");
makeCheckbox($pdf,$Left_Margin+420,$Ypos-100);
$pdf->addText($Left_Margin+420,$Ypos-100,$Default_Size,"    Units");
boxNumber($pdf,$Ypos,5);

$Ypos-=110;
$pdf->SetDrawColor(192,192,255);
for ( $i=1; $i<9; $i++ )
    {
	$pdf->Line($Left_Margin+172,$Ypos-15*$i,
		   $Right_Box-2,$Ypos-15*$i);
    }

bodyTextColor($pdf);
$pdf->addText($Left_Margin+15,$Ypos-20,$Default_Size,
	      "Resource Mission/Description:");
bodyDrawColor($pdf);boxNumber($pdf,$Ypos,6);
addRequired($pdf,$Ypos);

$Ypos-=135;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+15,$Ypos-20,$Default_Size,
	      "Requested Reporting Location:");
boxNumber($pdf,$Ypos,7);

$Ypos-=30;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+70,$Ypos-20,$Default_Size,
	      "Suggested Sources:");
$pdf->SetDrawColor(192,192,255);
for ( $i=1; $i<5; $i++ )
    {
	$pdf->Line($Left_Margin+172,$Ypos-2-15*$i,
		   $Right_Box-2,$Ypos-2-15*$i);
    }
bodyDrawColor($pdf);boxNumber($pdf,$Ypos,8);
addRequired($pdf,$Ypos);

$Ypos-=80;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+90,$Ypos-20,$Default_Size,
	      "Date/Time Due:");
boxNumber($pdf,$Ypos,9);
addRequired($pdf,$Ypos);

$Ypos-=30;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+12,$Ypos-20,$Default_Size,
	      "Associated Task Assignment #:");
boxNumber($pdf,$Ypos,10);

$Ypos-=30;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+95,$Ypos-20,$Default_Size,
	      "IAP Objective:");
$pdf->Line($Left_Margin+5,$Ypos-30,$Right_Box,$Ypos-30);
boxNumber($pdf,$Ypos,11);

$Ypos-=30;
$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+95,$Ypos-20,$Default_Size,
	      "IAP Strategy:");
$pdf->Line($Left_Margin+5,$Ypos-30,$Right_Box,$Ypos-30);
boxNumber($pdf,$Ypos,12);



/*****************************************************************************/
variableTextColor( $pdf );
/* Incident Name */
$pdf->addText($Left_Margin+245,647,$Subhead_Size,
	      "TRN-2018-04-28-Auxcomm-Voice-Messaging-Drill");

if ( $Example )
    {

	include ('RRdetails.inc');

	/* Date/time filed */
	$pdf->addText($Left_Margin+385,737,$Subhead_Size,
		      "0800L                      28 APR");

	/* Typed */
	if ( $Typed )
	    $pdf->addText($Left_Margin+200,575,$Subhead_Size,"X");
	else
	    $pdf->addText($Left_Margin+380,575,$Subhead_Size,"X");

	/* Resource Name */
	$pdf->addText($Left_Margin+185,548,$Subhead_Size,$Resource_Name);
	/* Quantity */
	$pdf->addText($Left_Margin+185,518,$Subhead_Size,$Quantity);
	/* Units */
	$pdf->addText($Left_Margin+180+120*intval($Units/5),486-20*($Units%5),
		      $Subhead_Size,"X");
	/* Resource Mission/Description */
	warnTextColor($pdf);
	$pdf->addText($Left_Margin+185,382,16,
		      "              THIS IS AN EXERCISE");
	variableTextColor( $pdf );
	$pdf->addText($Left_Margin+185,337,$Subhead_Size,$Mission[1]);
	$pdf->addText($Left_Margin+185,322,$Subhead_Size,$Mission[2]);
	$pdf->addText($Left_Margin+185,307,$Subhead_Size,$Mission[3]);
	$pdf->addText($Left_Margin+185,292,$Subhead_Size,$Mission[4]);
	/* Reporting Location */
	$pdf->addText($Left_Margin+185,242,$Subhead_Size,$Reporting_Location);
	/* Suggested sources */
	$pdf->addText($Left_Margin+185,218,$Subhead_Size,$Sources[1]);
	$pdf->addText($Left_Margin+185,218-15,$Subhead_Size,$Sources[2]);
	$pdf->addText($Left_Margin+185,218-30,$Subhead_Size,$Sources[3]);
	/* Date/Time Due */
	$pdf->addText($Left_Margin+185,130,$Subhead_Size,$DateTime);
	/* Associated task Assignment # */
	/* IAP Objective */
	$pdf->addText($Left_Margin+185,70,$Subhead_Size,$Objective);

    }



bodyTextColor($pdf);
/*****************************************************************************/




NextPage($pdf);
headingFont( $pdf );
centerText($pdf,0,$Right_Box,$Top_Box+20,16,
	   "RESOURCE REQUEST (continued)");

subheadFont( $pdf );
$Ypos=$Top_Box-50;


$pdf->Line($Left_Margin+5,$Ypos,$Right_Box,$Ypos);
$pdf->addText($Left_Margin+95,$Ypos-20,$Default_Size,
	      "IAP Tactic:");
$pdf->Line($Left_Margin+5,$Ypos-30,$Right_Box,$Ypos-30);
boxNumber($pdf,$Ypos,13);


$Ypos+=20;
$pdf->Line($Left_Margin+170,$Ypos-20,$Left_Margin+170,
	   $Ypos-110);
$pdf->Line($Left_Margin+5,$Ypos-20,$Left_Margin+5,
	   $Ypos-110 /*$Page_Height+95$Bottom_Margin*/);

$pdf->Line($Right_Box,$Ypos-20,$Right_Box,
	   $Ypos-110 /*$Page_Height+95$Bottom_Margin*/);
$pdf->Line($Left_Margin+5,$Ypos-50,$Right_Box,$Ypos-50);
$pdf->addText($Left_Margin+85,$Ypos-70,$Default_Size,
	      "Point of Contact:");

$pdf->Line($Left_Margin+5,$Ypos-80,$Right_Box,$Ypos-80);
$pdf->addText($Left_Margin+45,$Ypos-100,$Default_Size,
	      "Point of Contact Number:");
$pdf->Line($Left_Margin+5,$Ypos-110,$Right_Box,$Ypos-110);
boxNumber($pdf,$Ypos-50,14);
boxNumber($pdf,$Ypos-80,15);



$Ypos-=30;
$pdf->SetFillColor(230,230,230);
$pdf->Rect(35,245,552,26,'F');
centerText($pdf,$Left_Margin,$Right_Box,$Ypos-135,$Subhead_Size,
	   "Attachments");
$pdf->Rect(35,275,552,26,'N');
$pdf->addText($Left_Margin+10,$Ypos-165,10,
	      "If attachments necessary, send separately");
$pdf->Rect(35,309,552,26,'F');
centerText($pdf,$Left_Margin,$Right_Box,$Ypos-200,$Subhead_Size,
	   "Fullfillment Details");


$pdf->Line($Left_Margin+170,$Ypos-220,$Left_Margin+170,
	   $Ypos-250);
$pdf->Line($Left_Margin+5,$Ypos-220,$Left_Margin+5,
	   $Ypos-250 /*$Page_Height+95$Bottom_Margin*/);
$pdf->Line($Right_Box,$Ypos-220,$Right_Box,
	   $Ypos-250 /*$Page_Height+95$Bottom_Margin*/);
$pdf->Line($Left_Margin+5,$Ypos-220,$Right_Box,
	   $Ypos-220);
$pdf->Line($Left_Margin+5,$Ypos-250,$Right_Box,
	   $Ypos-250);
$pdf->addText($Left_Margin+100,$Ypos-240,10,
	      "Assigned To:");
$pdf->SetTextColor(192,192,192);
$pdf->addText($Left_Margin+370,$Ypos-240,8,
	      "If unknown SEOC LOG Logistics Section Chief");
bodyDrawColor($pdf);
$pdf->SetTextColor(0,196,0);
//$pdf->addText($Left_Margin+172,$Ypos-88,8,"14.");
$pdf->addText($Left_Margin+172,$Ypos-228,8,"16.");
bodyTextColor($pdf);

$pdf->Rect(35,388,552,26,'F');
$pdf->SetTextColor(0,0,0);  

centerText($pdf,$Left_Margin,$Right_Box,$Ypos-280,$Subhead_Size,
	   "Instructions");
$pdf->Rect(35,418,552,170,'N');
defaultFont($pdf);

$Ypos-=303;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "Assign a unique message number and be sure to notify the receiving station that this is a Resource Request. Your message number should not be the ");

$Ypos -= 10;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "same as any message number you have sent within the past month. Suggestion is to use sequential numbers starting annually, or sequential within the");

$Ypos -= 10;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "month preceded by the month number.  Unlike National Traffic System practice, dates and times are always local, not UTC.");

$Ypos-=20;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "At the top of the form record the date and time the message was received from emergency management. At the bottom, the time transmitted.");

$Ypos-=20;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "When sending over voice circuits, do not send the field description, but rather the field number only if it is known that the recipient has a blank form.");

$Ypos-=10;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "For Example:");

$Ypos-=10;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "One mixed group tango romeo november dash two zero one seven dash one two dash one nine dash northern initials mike india dash epidemic.");

$Ypos-=10;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "Two yes, three shelter manager type two, four figures three, five each, six  ...");

$Ypos-=20;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "If you do not have access to MI-CIMS, give enough description of the incident that the receiving station can select the correct incident.");

$Ypos-=20;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "Always use proper phonetics and remember to speak slowly and clearly, the receiving station must write everything down.");

$Ypos-=20;
$pdf->addText($Left_Margin+8,$Ypos,8,
	      "On NBEMS circuits, use the proper flmsg form and be sure the receiving station has the form available.");


//$pdf->addText($Left_Margin+330,$Ypos-12,$Subhead_Size,"3.Date/Time Initiated:");
defaultFont( $pdf );
RGfooter($pdf);



/*****************************************************************************/
if ( $Example )
    {
	variableTextColor($pdf);
	subheadFont( $pdf );

	/* Point of Contact */
	$pdf->addText($Left_Margin+185,628,$Subhead_Size,$POC);
	/* Point of Contact Number */
	$pdf->addText($Left_Margin+185,598,$Subhead_Size,$POCnum);
	/* Assigned To */
	$pdf->addText($Left_Margin+185,428,$Subhead_Size,$Assigned);
    }



bodyTextColor($pdf);
/*****************************************************************************/



$pdf->_out('/Modified '.$pdf->_textstring('D:'.date('YmdHis')));

$pdf->stream()

?>
