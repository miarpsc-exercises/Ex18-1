<?php
$Typed=1;
$Resource_Name = "Airborne Communications Relay Team (Fixed-Wing) Type 4";
$Quantity = 1;
$Units = 12;
$Mission[1]="Provide communications support to assist surveilance";
$Mission[2]="efforts around the community of Pullman.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="711 60th St, Pullman, MI 49450 M86";
$Sources[1]="CAP";
$Sources[2]="MSP";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "4. Maintain operational awareness to the extent possible";
$POC="Sabah van Leeuwen";
$POCnum="269-236-6890";
$Assigned="SEOC LOG Logistics Section Chief";
?>
