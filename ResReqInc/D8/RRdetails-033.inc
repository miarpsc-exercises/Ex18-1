<?php
$Typed=1;
$Resource_Name = "EOC Management Support Team Type 2";
$Quantity = 1;
$Units = 12;
$Mission[1]="Emergency operations support at Kincheloe";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="4657 W Industrial Park Dr Kincheloe MI 49788-1598";
$Sources[1]="Region 8 IMT";
$Sources[2]="Region 7 IMT";
$Sources[3]="EMHSD";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="M. Robbins";
$POCnum="906-495-3303";
$Assigned="SEOC LOG Logistics Section Chief";
?>
