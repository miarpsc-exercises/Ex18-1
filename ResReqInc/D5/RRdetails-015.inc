<?php
$Typed=1;
$Resource_Name = "Airborne Communications Relay Team (Fixed-Wing) Type 4";
$Quantity = 1;
$Units = 12;
$Mission[1]="Support surveillence of the Yankee Springs";
$Mission[2]="Recreation Area";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="2505 Murphy Dr, Hastings, MI 49058 9D9";
$Sources[1]="Civil Air Patrol";
$Sources[2]="Michigan State Police";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "4. Maintain operational awareness to the extent possible";
$POC="Colonel Kentucky Fried";
$POCnum="269-945-2468";
$Assigned="SEOC LOG Logistics Section Chief";
?>
