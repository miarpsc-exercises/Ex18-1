<?php

function RGheader( $pdf )
{
$y = 30;
$pdf->SetFillColor(255,255,232);
$pdf->Rect(30,$y,552,35,'F');
$pdf->SetFillColor(0,128,0);
$pdf->Rect(30,$y,552,10,'F');
$pdf->SetDrawColor(0,128,0);
$pdf->Rect(30,$y,552,35,'N');
$pdf->SetTextColor(255,255,232);
$pdf->SelectFont("helvetica");
$pdf->addText(35,$y+725,8,"Message Number");
$pdf->addText(200,$y+725,8,"Originating Station");
$pdf->addText(410,$y+725,8,"Time Filed");
$pdf->addText(535,$y+725,8,"Date");
$pdf->Line(180,$y+732,180,$y+698);
$pdf->Line(400,$y+732,400,$y+698);
$pdf->SetTextColor(0,0,0);
$pdf->SetDrawColor(0,0,0);
}

function RGfooter( $pdf )
{
$y = 720;
$pdf->SetFillColor(255,255,232);
$pdf->Rect(30,$y,552,35,'F');
$pdf->SetFillColor(0,128,0);
$pdf->Rect(30,$y,552,10,'F');
$pdf->SetDrawColor(0,128,0);
$pdf->Rect(30,$y,552,35,'N');
$pdf->SetTextColor(255,255,232);
$pdf->SelectFont("helvetica");
$pdf->addText(200,64,8,"Receiving Station Call");
$pdf->addText(64,64,8,"Sending Operator Call");
$pdf->addText(360,64,8,"Time Sent");
$pdf->addText(535,64,8,"Date Sent");
$pdf->Line(180,37,180,57+14);
$pdf->Line(350,37,350,57+14);
$pdf->SetTextColor(0,0,0);
$pdf->SetDrawColor(0,0,0);
}

?>
