<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Evacuate residents stranded on an island";
$Mission[2]="in Richmond Lake";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="7825 N Woodbridge Rd, Brohman, MI 49312";
$Sources[1]="LSC";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Sgt. Chester Baumgartner";
$POCnum="231-689-2295";
$Assigned="SEOC LOG Logistics Section Chief";
?>
