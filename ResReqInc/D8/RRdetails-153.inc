<?php
$Typed=1;
$Resource_Name = "Field Kitchen Unit Type 4";
$Quantity = 2;
$Units = 4;
$Mission[1]="Provide food to residents of Gulliver and Parkington";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="2804W US-2, Gulliver, MI 49840";
$Sources[1]="American Red Cross";
$Sources[2]="First Baptist Church";
$Sources[3]="";
$DateTime="2018-04-28 09:45";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Barbarella Duran-Duran";
$POCnum="906-283-3114";
$Assigned="SEOC LOG Logistics Section Chief";
?>
