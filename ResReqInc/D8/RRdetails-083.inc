<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Arrange evacuation of stranded tourists from";
$Mission[2]="Windigo on Isle Royale";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="5189 Garden City Rd, Mohawk, MI 49950";
$Sources[1]="EMHSD";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Lt. Joseph A. Banks III";
$POCnum="906-337-0527";
$Assigned="SEOC LOG Logistics Section Chief";
?>
