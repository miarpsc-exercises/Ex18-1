<?php
$Typed=1;
$Resource_Name = "Electronic Boards Variable Message Signs (VMS)-Type 4";
$Quantity = 2;
$Units = 4;
$Mission[1]="Direct traffic around a hazardous materials incident";
$Mission[2]="on M-30 near Edenville which will take several days";
$Mission[3]="to clear.";
$Mission[4]="";
$Reporting_Location="5891 State Highway 30, Beaverton, MI 48612";
$Sources[1]="MDOT";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Sgt. Herman E. Mellville";
$POCnum="989-689-4555";
$Assigned="SEOC LOG Logistics Section Chief";
?>
