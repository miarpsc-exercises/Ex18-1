<?php
$Typed=1;
$Resource_Name = "Electronic Boards Variable Message Signs (VMS)-Type 4";
$Quantity = 2;
$Units = 4;
$Mission[1]="Detour traffic around a damaged section of";
$Mission[2]="US-24.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="1415 S Telegraph Rd, Bloomfield Hills, MI 48302";
$Sources[1]="Michigan Department of transportation";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Deputy Ralph Tomaszewski";
$POCnum="248-745-7038";
$Assigned="SEOC LOG Logistics Section Chief";
?>
