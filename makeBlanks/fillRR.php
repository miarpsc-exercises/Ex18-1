<?php

$file="RRdetails-t01.inc";

$res=array("Ambulance Strike Team Type 4",
	   "Airborne Communications Relay Team (Fixed-Wing) Type 4",
	   "EOC Management Support Team Type 2",
	   "Evacuation Coordination Team Type 1",
	   "Field Kitchen Unit Type 4",
	   "Paramedic Type 1",
	   "Law Enforcement Patrol Team (Strike Team) Type 4",
	   "Shelter Management Team Type 2",
	   "Electronic Boards Variable Message Signs (VMS)-Type 4",
	   "Utility Systems Reconstruction Manager-Type 1",
	   "Assistant Public Works Director - Logistics-Type 1",
	   "Species Specialist-Type 1",
	   "Animal Protection: Small Animal Sheltering Team-Type 3");
$qtys=array(1,1,1,1,2,4,3,3,2,1,1,1,2);
$units=array(10,12,12,12,4,14,10,12,4,4,4,4,12);
$objid=array(1,4,1,1,3,1,2,3,1,6,6,1,3);
$objs=array("",
"1. Maintain safety of life and property",
"2. Maintain public order",
"3. Provide food and shelter to those citizens requiring it",
"4. Maintain operational awareness to the extent possible",
"5. Identify source of problem",
"6. Restore utilities");


echo "#!/bin/sh\n";
$resnum=0;
for ( $i=0; $i<5; $i++ )
    {
        $resnum= rand(0,12);
	$num=101+$i;
	$file = "RRdetails-t" . $num . ".inc";
	echo "# $i $num $resnum \n";
	$resource=$res[$resnum];
	$qty=$qtys[$resnum];
	$unit=$units[$resnum];
	$obj=$objs[$objid[$resnum]];

	echo 'cp RRdetails-template.inc ' . $file . "\n";
	echo 'sed -i "s?Resource_Name = \"\"?Resource_Name = \"' .
	    $resource . '\"?" ' . $file . "\n";
	echo 'sed -i "s?Quantity = 5?Quantity = ' . $qty . '?" ' . $file . "\n";
	echo 'sed -i "s?Units = 4?Units = ' . $unit . '?" ' . $file . "\n";
	echo 'sed -i "s?$Objective = \"\"?$Objective = \"' . $obj . 
	    '\"?" ' . $file . "\n";
    }

?>
