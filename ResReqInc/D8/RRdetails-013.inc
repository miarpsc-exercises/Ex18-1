<?php
$Typed=1;
$Resource_Name = "Animal Protection: Small Animal Sheltering Team";
$Quantity = 1;
$Units = 12;
$Mission[1]="Provide sheltering/warmth for a number of pets";
$Mission[2]="whose owners are unable to provide adequate warmth.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="Cozy Inn, US-41, Covington, MI 49919";
$Sources[1]="Michigan Department of Agriculture and Rural Development";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective="1. Maintain safety of life and property";
$POC="Ethyl R. Mermain";
$POCnum="(906) 272-9876";
$Assigned="SEOC OPS MDARD";
?>
