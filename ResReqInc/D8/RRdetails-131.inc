<?php
$Typed=1;
$Resource_Name = "Utility Systems Reconstruction Manager-Type 1";
$Quantity = 1;
$Units = 4;
$Mission[1]="Restore utilities in and around the villages";
$Mission[2]="of Nonesuch and Merriweather";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="115 Hoop N Hollar Rd, Merriweather, MI 49947";
$Sources[1]="";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "6. Restore utilities";
$POC="Nikolina Lofthus";
$POCnum="906-575-5555";
$Assigned="SEOC LOG Logistics Section Chief";
?>
