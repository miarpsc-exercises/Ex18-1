<?php
$Typed=1;
$Resource_Name = "Field Kitchen Unit Type 4";
$Quantity = 2;
$Units = 4;
$Mission[1]="Feed local residents who have been without power";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="10381 Bailey Dr NE, Lowell, MI 49331";
$Sources[1]="First Baptist Church";
$Sources[2]="American Red Cross";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Mr. J. S. Nawrotski";
$POCnum="616-897-5671";
$Assigned="SEOC LOG Logistics Section Chief";
?>
