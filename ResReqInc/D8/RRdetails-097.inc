<?php
$Typed=1;
$Resource_Name = "Law Enforcement Patrol Team (Strike Team) Type 4";
$Quantity = 3;
$Units = 10;
$Mission[1]="Law enforcement assistance required to quiet";
$Mission[2]="rioting around Danny's Auto Value.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="W14006 Melville St, Engadine, MI 49827";
$Sources[1]="Michigan State Police";
$Sources[2]="Mackinac County Sheriff Department";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "2. Maintain public order";
$POC="Chief Harold Sigurd Kjeldahl";
$POCnum="906-477-6311";
$Assigned="SEOC LOG Logistics Section Chief";
?>
