<?php
$Typed=1;
$Resource_Name = "Airborne Communications Relay Team (Fixed-Wing) Type 4";
$Quantity = 1;
$Units = 12;
$Mission[1]="Airborne repeater required to support operations";
$Mission[2]="in and around the Wolcott Mill Historical Center.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="17180 30 Mile Rd, Ray, MI 48096 MI50";
$Sources[1]="Civil Air Patrol";
$Sources[2]="Michigan State Police";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "4. Maintain operational awareness to the extent possible";
$POC="Captain Oddvar Hoel";
$POCnum="586-752-4102";
$Assigned="SEOC LOG Logistics Section Chief";
?>
