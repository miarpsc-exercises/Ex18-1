<?php
$Typed=1;
$Resource_Name = "Paramedic Type 1";
$Quantity = 4;
$Units = 14;
$Mission[1]="Treat a large number of patients suffering from";
$Mission[2]="severe writer's cramp and Kirtland Community College";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="10775 N St Helen Rd, Roscommon, MI 48653";
$Sources[1]="Region 7 Healthcare Coalition";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Prof. A. H. Smartguy";
$POCnum="989-275-5020";
$Assigned="SEOC LOG Logistics Section Chief";
?>
