<?php
$Typed=1;
$Resource_Name = "Airborne Communications Relay Team (Fixed-Wing) Type 4";
$Quantity = 1;
$Units = 12;
$Mission[1]="Provide airborne 800 MHz repeater to law";
$Mission[2]="enforcement efforts along the Grand River";
$Mission[3]="in Ionia County MI";
$Mission[4]="";
$Reporting_Location="3147 S State Rd, Ionia, MI 48846 - Y70";
$Sources[1]="Civil Air Patrol";
$Sources[2]="Michigan State Police";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "4. Maintain operational awareness to the extent possible";
$POC="Col. K. Sanders";
$POCnum="616-527-9070";
$Assigned="SEOC LOG Logistics Section Chief";
?>
