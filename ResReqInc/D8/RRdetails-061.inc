<?php
$Typed=1;
$Resource_Name = "Assistant Public Works Director - Logistics-Type 1";
$Quantity = 1;
$Units = 4;
$Mission[1]="Assist in service restoration to the Stanton, Liminga";
$Mission[2]="and Oskar areas";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="16841 Fire Hall Rd, Houghton, MI 49931";
$Sources[1]="MDOT";
$Sources[2]="LSC";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "6. Restore utilities";
$POC="Melissa V. Maarschalkerweerd";
$POCnum="906-482-2026";
$Assigned="SEOC LOG Logistics Section Chief";
?>
