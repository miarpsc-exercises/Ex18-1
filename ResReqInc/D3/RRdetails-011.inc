<?php
$Typed=1;
$Resource_Name = "Utility Systems Reconstruction Manager-Type 1";
$Quantity = 1;
$Units = 4;
$Mission[1]="Restore utility systems to Whites Beach";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="5450 Sturman Rd, Standish, MI 48658";
$Sources[1]="Consumers Energy";
$Sources[2]="MPSC";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "6. Restore utilities";
$POC="Abedabun Jiibayaabooz";
$POCnum="989-846-6277";
$Assigned="SEOC LOG Logistics Section Chief";
?>
