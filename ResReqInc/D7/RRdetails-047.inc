<?php
$Typed=1;
$Resource_Name = "Ambulance Strike Team Type 4";
$Quantity = 1;
$Units = 10;
$Mission[1]="Need to transport multiple patients from the";
$Mission[2]="smokehouse to McLaren";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="3459 U.S. 31, Brutus, MI 49716";
$Sources[1]="Region 7 Healthcare coalition";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Louisa May Alstad";
$POCnum="231-529-6574";
$Assigned="SEOC LOG Logistics Section Chief";
?>
