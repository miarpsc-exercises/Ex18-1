<?php
$Typed=1;
$Resource_Name = "Shelter Management Team Type 2";
$Quantity = 3;
$Units = 12;
$Mission[1]="Provide shelter for homeowners displaced by";
$Mission[2]="flooding in the Big Sable River";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="453-1 W Free Soil Rd, Free Soil, MI 49411";
$Sources[1]="American Red Cross";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 11:00";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Genowefa Sokolowski";
$POCnum="231-464-5534";
$Assigned="SEOC LOG Logistics Section Chief";
?>
