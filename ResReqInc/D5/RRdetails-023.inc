<?php
$Typed=1;
$Resource_Name = "Paramedic Type 1";
$Quantity = 4;
$Units = 14;
$Mission[1]="Paramedic assistance required in the township of";
$Mission[2]="Girard due to a large riot at The Gun Rack. Staging";
$Mission[3]="at Girard United Methodist Church.";
$Mission[4]="";
$Reporting_Location="990 Marshall Rd, Coldwater, MI 49036";
$Sources[1]="Region 5 Healthcare Coalition";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Ms. Anke Hochstedler";
$POCnum="517-279-9418";
$Assigned="SEOC LOG Logistics Section Chief";
?>
