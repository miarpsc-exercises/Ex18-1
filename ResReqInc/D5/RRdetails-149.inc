<?php
$Typed=1;
$Resource_Name = "Animal Protection: Small Animal Sheltering Team-Type 3";
$Quantity = 2;
$Units = 12;
$Mission[1]="Provide shelter for pets of displaced individuals";
$Mission[2]="arriving at the Centreville shelter";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="190 Hogan St, Centreville, MI 49032";
$Sources[1]="Michigan Department of Agriculture and Rural Development";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Hiram S. Broadhurst";
$POCnum="269-467-5210";
$Assigned="SEOC LOG Logistics Section Chief";
?>
