<?php
$Typed=1;
$Resource_Name = "Electronic Boards Variable Message Signs (VMS)-Type 4";
$Quantity = 6;
$Units = 4;
$Mission[1]="Direct traffic around hazardous materials incident";
$Mission[2]="at Battle Creek Transit at Cass and Michigan";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="411 Champion St, Battle Creek, MI 49037";
$Sources[1]="MDOT";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Sgt. Wilbur Small";
$POCnum="269-965-3070";
$Assigned="SEOC LOG Logistics Section Chief";
?>
