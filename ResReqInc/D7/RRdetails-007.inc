<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Evacuate campers from the Thunder Bay River";
$Mission[2]="State Forest";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="2850 Werth Rd, Alpena, MI 49707";
$Sources[1]="LSC";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 10:30";
$Objective = "1. Maintain safety of life and property";
$POC="Sgt. Joe Botz";
$POCnum="989-354-4798";
$Assigned="SEOC LOG Logistics Section Chief";
?>
