<?php
$Typed=1;
$Resource_Name = "Shelter Management Team Type 2";
$Quantity = 3;
$Units = 12;
$Mission[1]="Set up shelters for citizens without power";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="1588-1644 W Trask Lake Rd, Barton City, MI 48705";
$Sources[1]="American Red Cross";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Jacob A. Hochstetler";
$POCnum="989-736-3174";
$Assigned="SEOC LOG Logistics Section Chief";
?>
