<?php
$Typed=1;
$Resource_Name = "Species Specialist-Type 1";
$Quantity = 1;
$Units = 4;
$Mission[1]="Require expertise in treating a number of";
$Mission[2]="Black Roughneck monitor lizards and a";
$Mission[3]="Spiny-tailed Goanna.";
$Mission[4]="";
$Reporting_Location="4699 Mackinaw Trail, Le Roy, MI 49655";
$Sources[1]="MDARD";
$Sources[2]="Pol Veterinary Services ";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Yngvild Albrecht";
$POCnum="231-768-4471";
$Assigned="SEOC LOG Logistics Section Chief";
?>
