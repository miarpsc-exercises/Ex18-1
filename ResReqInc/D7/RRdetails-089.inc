<?php
$Typed=1;
$Resource_Name = "Utility Systems Reconstruction Manager-Type 1";
$Quantity = 1;
$Units = 4;
$Mission[1]="Organize local labor in order to restore";
$Mission[2]="utility systems in Omena and Peshawbestown";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="12819 E Tatch Rd, Omena, MI 49674";
$Sources[1]="MPSC";
$Sources[2]="LSC";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "6. Restore utilities";
$POC="Ms. Mandy Moore";
$POCnum="231-386-9787";
$Assigned="SEOC LOG Logistics Section Chief";
?>
