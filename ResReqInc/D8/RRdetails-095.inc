<?php
$Typed=1;
$Resource_Name = "Animal Protection: Small Animal Sheltering Team-Type 1";
$Quantity = 2;
$Units = 12;
$Mission[1]="Provide shelter to 43 dogs in danger of exposure";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="14785 Co Rd 415, McMillan, MI 49853";
$Sources[1]="MDARD";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="A. B. Stoddard";
$POCnum="906-748-0513";
$Assigned="SEOC LOG Logistics Section Chief";
?>
