<?php
$Typed=1;
$Resource_Name = "Electronic Boards Variable Message Signs (VMS)-Type 4";
$Quantity = 2;
$Units = 4;
$Mission[1]="Detour traffic around US-41 construction near";
$Mission[2]="Nadeau, MI";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="151 US-41, Carney, MI 49812";
$Sources[1]="MDOT";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Ingeborg Langeland";
$POCnum="906-639-2435";
$Assigned="SEOC LOG Logistics Section Chief";
?>
