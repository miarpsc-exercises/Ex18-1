<?php
$Typed=1;
$Resource_Name = "Shelter Management Team Type 2";
$Quantity = 2;
$Units = 12;
$Mission[1]="Establish congregate care centers at the Lawrence";
$Mission[2]="Elementary School and the Paw Paw Middle School";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="714 W St Joseph St, Lawrence, MI 49064";
$Sources[1]="American Red Cross";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Lt. Melanie X. Smyth";
$POCnum="269-415-5570";
$Assigned="SEOC LOG Logistics Section Chief";
?>
