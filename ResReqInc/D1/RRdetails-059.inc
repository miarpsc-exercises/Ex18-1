<?php
$Typed=1;
$Resource_Name = "Animal Protection: Small Animal Sheltering Team-Type 3";
$Quantity = 2;
$Units = 12;
$Mission[1]="Temporarily shelter a large number of red foxes";
$Mission[2]="to prevent disrupting the golf tournament";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="3835 Bird Lake Rd S, Osseo, MI 49266";
$Sources[1]="MDARD";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="George Henry Wallenbeck";
$POCnum="517-523-3990";
$Assigned="SEOC LOG Logistics Section Chief";
?>
