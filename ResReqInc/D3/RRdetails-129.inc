<?php
$Typed=1;
$Resource_Name = "Paramedic Type 1";
$Quantity = 4;
$Units = 14;
$Mission[1]="Require assistance in treating and transporting";
$Mission[2]="individuals injured at Blair Linda Classy Curl.";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="3380 Lupton Rd, Lupton, MI 48635";
$Sources[1]="Region 3 Healthcare Coalition";
$Sources[2]="Michigan Department of Health and Human Services";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Rafaele Czarnecki";
$POCnum="989-473-2101";
$Assigned="SEOC LOG Logistics Section Chief";
?>
