<?php
$Typed=1;
$Resource_Name = "Wilderness Search and Rescue Team Type 2";
$Quantity = 5;
$Units = 12;
$Mission[1]="Locate a troop of Cub Scouts (ages 8-11) lost";
$Mission[2]="in the woods northeast of Fife lake.";
$Mission[4]="LKP 16T640470 Easting, 4937936 Northing";
$Mission[3]="";
$Reporting_Location="372 Woodsedge Dr, Fife Lake MI 49633";
$Sources[1]="Michigan State Police SOD";
$Sources[2]="Kent County Search and Rescue";
$Sources[3]="Michigan Search and Rescue";
$DateTime="2018-04-28 09:00";
$Objective="1. Maintain safety of life and property";
$POC="Samuel F. B. Morse";
$POCnum="(261) 234-0675";
$Assigned=" ";
?>
