<?php
$Typed=1;
$Resource_Name = "Shelter Management Team Type 2";
$Quantity = 3;
$Units = 12;
$Mission[1]="Set up several shelters in the area of Durand, MI";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="9575 E Monroe Rd, Durand, MI 48429";
$Sources[1]="American Red Cross";
$Sources[2]="MDHHS";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Betsy L. Ross";
$POCnum="989-288-8733";
$Assigned="SEOC LOG Logistics Section Chief";
?>
