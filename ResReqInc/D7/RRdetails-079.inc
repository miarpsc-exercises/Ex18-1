<?php
$Typed=0;
$Resource_Name = "Auxcomm VHF Operator";
$Quantity = 5;
$Units = 4;
$Mission[1]="Provide field communications to search teams looking for a";
$Mission[2]="pack of Cub Scouts lost northeast of the staging area.";
$Mission[3]="Operators must be equipped with VHF comms and be physically";
$Mission[4]="capable of following field teams through the woods.";
$Reporting_Location="372 Woodsedge Dr, Fife Lake MI 49633";
$Sources[1]="Grand Traverse ARES/RACES";
$Sources[2]="CCEOEM";
$Sources[3]="Benzie County ARES";
$DateTime="2018-04-28 09:00";
$Objective="1. Maintain safety of life and property";
$POC="Samuel F. B. Morse";
$POCnum="(261) 234-0675";
$Assigned="County of Grand Traverse LOG RACES";
?>
