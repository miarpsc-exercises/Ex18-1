#!/bin/sh
DIR="/home/jjmcd/Documents/ARPSC/Exercises/Ex18-1/Ex18-1"
#LIST="003 009 013 017 021 043 049 055 079"
LIST="BattleCreek"
for REQ in $LIST; do
    cp $DIR/RRdetails-$REQ.inc $DIR/RRdetails.inc
    php $DIR/makeResourceRequest.php >$DIR/DrillResReq-$REQ.pdf
    rm $DIR/RRdetails.inc
done
