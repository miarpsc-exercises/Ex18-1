<?php
$Typed=1;
$Resource_Name = "EOC Management Support Team Type 2";
$Quantity = 1;
$Units = 12;
$Mission[1]="Provide EOC support and expertise to the";
$Mission[2]="Calhoun County Emergency Operations Center";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="161 East Michigan Ave. Battle Creek, MI 49014";
$Sources[1]="District 5 IMT";
$Sources[2]="Lt. Ken High";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Durk Dunham";
$POCnum="269-969-6430";
$Assigned="SEOC LOG Logistics Section Chief";
?>
