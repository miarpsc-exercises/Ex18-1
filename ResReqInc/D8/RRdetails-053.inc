<?php
$Typed=1;
$Resource_Name = "Shelter Management Team Type 2";
$Quantity = 3;
$Units = 12;
$Mission[1]="Set up three shelters in and around Wakefield, MI.";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="500 Indianhead Rd, Wakefield, MI 49968";
$Sources[1]="American Red Cross";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Melissa Manchester";
$POCnum="906-224-8531";
$Assigned="SEOC LOG Logistics Section Chief";
?>
