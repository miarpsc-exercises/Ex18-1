<?php
$Typed=1;
$Resource_Name = "Field Kitchen Unit Type 4";
$Quantity = 2;
$Units = 4;
$Mission[1]="Feed residents who have been without electricity";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="4115 E Chandler St, Carsonville, MI 48419";
$Sources[1]="American Red Cross";
$Sources[2]="First baptist Church";
$Sources[3]="";
$DateTime="2018-04-28 10:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Timofey X. Yvacheslav";
$POCnum="810-657-8318";
$Assigned="SEOC LOG Logistics Section Chief";
?>
