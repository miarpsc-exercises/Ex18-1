<?php
$Typed=1;
$Resource_Name = "Ambulance Strike Team Type 4";
$Quantity = 1;
$Units = 10;
$Mission[1]="Support overwhelmed local ambulance companies";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="325 S Clinton St, Grand Ledge, MI 48837";
$Sources[1]="Michigan Department of Health and Human Services";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:00";
$Objective = "1. Maintain safety of life and property";
$POC="Chief Melissa A. Manly";
$POCnum="517-622-7935";
$Assigned="SEOC LOG Logistics Section Chief";
?>
