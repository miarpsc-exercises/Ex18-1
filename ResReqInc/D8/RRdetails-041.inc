<?php
$Typed=1;
$Resource_Name = "Evacuation Coordination Team Type 1";
$Quantity = 1;
$Units = 12;
$Mission[1]="Evacuate citizens lost in the Hayes";
$Mission[2]="Corn Maze";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="3474 Saint Nicholas Road, Rock, MI 49880";
$Sources[1]="LSC";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "1. Maintain safety of life and property";
$POC="Rutherford B. Hayes";
$POCnum="906-384-6700";
$Assigned="SEOC LOG Logistics Section Chief";
?>
