<?php
$Typed=1;
$Resource_Name = "Animal Protection: Small Animal Sheltering Team-Type 3";
$Quantity = 2;
$Units = 12;
$Mission[1]="Set up shelter for 132 at-risk squirrels";
$Mission[2]="";
$Mission[3]="";
$Mission[4]="";
$Reporting_Location="5016 Main St, Onekama, MI 49675";
$Sources[1]="Michigan Department of Agriculture and Rural Development";
$Sources[2]="";
$Sources[3]="";
$DateTime="2018-04-28 09:30";
$Objective = "3. Provide food and shelter to those citizens requiring it";
$POC="Nancy Jobaria";
$POCnum="231-889-5521";
$Assigned="SEOC LOG Logistics Section Chief";
?>
